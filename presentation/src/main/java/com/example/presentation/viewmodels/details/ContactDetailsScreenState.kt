package com.example.presentation.viewmodels.details

import com.example.core.database.entity.Contact
import com.example.presentation.mvibase.UiState
import com.example.presentation.util.ContactUiState

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
data class ContactDetailsScreenState(
    val isShowProgress: Boolean = false,
    val data: Contact? = null,
    val changedUiValue: ContactUiState = ContactUiState(),
    val enableEditableState: Boolean = false
) : UiState {

    companion object {
        fun initial() = ContactDetailsScreenState()
    }


}