package com.example.presentation.viewmodels.contacts

import androidx.lifecycle.viewModelScope
import com.example.core.data.AppResponse
import com.example.core.repository.ContactRepository
import com.example.presentation.mvibase.BaseViewModel
import com.example.presentation.mvibase.Reducer
import dagger.hilt.android.HiltAndroidApp
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
@HiltViewModel
class ContactsViewModel @Inject constructor(
    private val repository: ContactRepository,
) : BaseViewModel<ContactsScreenState, ContactsScreenEvent, ContactsScreenSideEffect>() {

    private val reducer = MainReducer(ContactsScreenState.initial())

    override val state: StateFlow<ContactsScreenState> = reducer.state
    override val sideEffect: Flow<ContactsScreenSideEffect> = reducer.sideEffect

    fun getAllContacts() = viewModelScope.launch(Dispatchers.IO) {
        when (val resp = repository.getAllContacts()) {
            is AppResponse.Error -> {
                sendEvent(ContactsScreenEvent.Error(throwable = resp.throwable))
            }

            is AppResponse.Success -> {
                sendEvent(ContactsScreenEvent.ShowData(items = resp.data))
            }
        }
    }

    fun navigateTo(route: String) {
        sendEvent(ContactsScreenEvent.NavigateTo(route = route))
    }

    private fun sendEvent(event: ContactsScreenEvent) {
        reducer.sendEvent(event)
    }

    private class MainReducer(initial: ContactsScreenState) :
        Reducer<ContactsScreenState, ContactsScreenEvent, ContactsScreenSideEffect>(initial) {
        override fun reduce(oldState: ContactsScreenState, event: ContactsScreenEvent) {
            when (event) {
                is ContactsScreenEvent.Error -> {
                    setSideEffect(ContactsScreenSideEffect.ShowError(message = event.throwable.message.toString()))
                    setState(
                        ContactsScreenState(
                            isShowProgress = false,
                            data = emptyList(),
                            error = null
                        )
                    )
                }

                ContactsScreenEvent.DeleteAll -> {
                    setSideEffect(ContactsScreenSideEffect.DeleteALlMessage)
                    setState(
                        ContactsScreenState(
                            isShowProgress = false,
                            data = emptyList(),
                            error = null
                        )
                    )
                }

                is ContactsScreenEvent.NavigateTo -> {
                    setSideEffect(ContactsScreenSideEffect.NavigateTo(event.route))
                }

                is ContactsScreenEvent.ShowData -> {
                    setState(
                        oldState.copy(
                            isShowProgress = false,
                            data = event.items,
                            error = null
                        )
                    )
                }
            }
        }

    }
}

