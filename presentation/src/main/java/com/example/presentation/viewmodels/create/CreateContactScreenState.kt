package com.example.presentation.viewmodels.create

import com.example.presentation.mvibase.UiState
import com.example.presentation.viewmodels.details.ContactDetailsScreenState

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
data class CreateContactScreenState(
    val isShowProgress: Boolean = false,
) : UiState {

    companion object {
        fun initial() = ContactDetailsScreenState(
            isShowProgress = false,
        )
    }


}