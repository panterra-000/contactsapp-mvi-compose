package com.example.presentation.util

import com.example.core.database.entity.Contact


data class ContactUiState(
    val id: Int = 0,
    val fullName: String = "",
    val mobile: String? = "",
    val email: String? = "",
    val img: String? = "",
    val actionEnable: Boolean = false
)

fun ContactUiState.toContact(): Contact = Contact(
    id = id,
    fullName = fullName,
    phone = mobile?.toLongOrNull() ?: 0,
    image = img,
    email = email
)

fun Contact.toContactUiState(actionEnable: Boolean = false): ContactUiState = ContactUiState(
    id = id,
    fullName = fullName,
    mobile = phone.toString(),
    email = email,
    actionEnable = actionEnable
)


fun ContactUiState.isValid(): Boolean {
    return fullName.isNotBlank()
}