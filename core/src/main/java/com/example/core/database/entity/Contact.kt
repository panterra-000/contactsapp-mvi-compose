package com.example.core.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
@Entity(tableName = "contact_table")
data class Contact(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    @ColumnInfo(name = "full_name")
    val fullName: String,
    @ColumnInfo(name = "phone_number")
    val phone: Long? = null,
    @ColumnInfo(name = "img")
    val image: String?,
    @ColumnInfo(name = "email")
    val email: String? = null
)
