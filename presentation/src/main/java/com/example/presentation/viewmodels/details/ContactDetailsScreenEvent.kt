package com.example.presentation.viewmodels.details

import androidx.compose.runtime.Immutable
import com.example.core.database.entity.Contact
import com.example.presentation.mvibase.UiEvent
import com.example.presentation.util.ContactUiState

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
@Immutable
sealed class ContactDetailsScreenEvent : UiEvent {

    data class Error(val throwable: Throwable) : ContactDetailsScreenEvent()

    object GetDetailsSend : ContactDetailsScreenEvent()
    data class TakenDetails(val contact: Contact) : ContactDetailsScreenEvent()

    object UpdateSend : ContactDetailsScreenEvent()
    object TakenUpdate : ContactDetailsScreenEvent()

    object DeleteSend : ContactDetailsScreenEvent()
    object TakenDeleted : ContactDetailsScreenEvent()

    data class ChangeValue(val changedValue: ContactUiState) : ContactDetailsScreenEvent()
    object NavigateUp : ContactDetailsScreenEvent()
    data class SetEditableState(val enable: Boolean) : ContactDetailsScreenEvent()

}