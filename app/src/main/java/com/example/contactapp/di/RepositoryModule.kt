package com.example.contactapp.di

import android.app.Application
import com.example.core.database.dao.ContactDao
import com.example.core.repository.ContactRepository
import com.example.core.repository.ContactRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
@Module
@InstallIn(SingletonComponent::class)
interface RepositoryModule {


    @Binds
    @Singleton
    fun getContactRepository(contactRepositoryImpl: ContactRepositoryImpl): ContactRepository
}