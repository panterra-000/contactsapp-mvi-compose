package com.example.core.repository

import com.example.core.data.AppResponse
import com.example.core.database.entity.Contact

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
interface ContactRepository {

    suspend fun addContact(contact: Contact): AppResponse<Long>

    suspend fun deleteContact(contact: Contact): AppResponse<Int>

    suspend fun editContact(contact: Contact): AppResponse<Int>

    suspend fun getAllContacts(): AppResponse<List<Contact>>

    suspend fun getContact(id: Int): AppResponse<Contact>
}