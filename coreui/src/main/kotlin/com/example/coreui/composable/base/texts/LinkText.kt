package com.example.coreui.composable.base.texts

import androidx.compose.foundation.clickable
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType.Companion.Text
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.sp
import com.example.coreui.theme.ContactAppTheme

@Composable
fun LinkTextView12Sp(text: String = "", onclick: () -> Unit) {

    Text(
        text = text,
        fontSize = 12.sp,
        color = ContactAppTheme.colors.primaryClickableText,
        style = TextStyle(textDecoration = TextDecoration.Underline),
        modifier = Modifier.clickable(onClick = onclick)
    )
}