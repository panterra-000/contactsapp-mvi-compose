package com.example.coreui.composable.base.seekbars

import androidx.compose.material3.Slider
import androidx.compose.material3.Text
import androidx.compose.runtime.*


@Composable
fun MySliderDemo() {
    var sliderPosition by remember { mutableStateOf(0f) }
    Text(text = sliderPosition.toString())
    Slider(value = sliderPosition,onValueChange = { sliderPosition = it })

}