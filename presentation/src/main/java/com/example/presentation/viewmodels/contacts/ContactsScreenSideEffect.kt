package com.example.presentation.viewmodels.contacts

import com.example.presentation.mvibase.UiSideEffect

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
sealed class ContactsScreenSideEffect : UiSideEffect {

    data class ShowError(val message: String) : ContactsScreenSideEffect()

    data class NavigateTo(val route: String) : ContactsScreenSideEffect()

    object DeleteALlMessage : ContactsScreenSideEffect()
}