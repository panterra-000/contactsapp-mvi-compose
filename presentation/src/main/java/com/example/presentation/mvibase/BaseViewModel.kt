package com.example.presentation.mvibase

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
abstract class BaseViewModel<S : UiState, E : UiEvent, SE : UiSideEffect> : ViewModel() {

    abstract val state: Flow<S>
    abstract val sideEffect: Flow<SE>


}
