package com.example.presentation.screens

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.coreui.R
import com.example.coreui.composable.base.ProgressBarWithBlock
import com.example.coreui.composable.base.Spacer12dp
import com.example.presentation.util.ContactUiState
import com.example.presentation.util.toContactUiState
import com.example.presentation.viewmodels.details.ContactDetailsScreenSideEffect
import com.example.presentation.viewmodels.details.ContactDetailsViewModel
import kotlinx.coroutines.flow.collect

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ContactDetailsScreen(
    navController: NavHostController, id: Int, viewModel: ContactDetailsViewModel = hiltViewModel()
) {

    val state by viewModel.state.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(key1 = Unit, block = {
        viewModel.getContactDetails(id)
    })

    LaunchedEffect(key1 = Unit, block = {
        viewModel.sideEffect.collect {
            when (it) {
                ContactDetailsScreenSideEffect.NavigateUp -> {
                    navController.navigateUp()
                }

                is ContactDetailsScreenSideEffect.ShowError -> {
                    Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                }

                ContactDetailsScreenSideEffect.Updated -> {
                    Toast.makeText(context, context.getString(R.string.updated_message), Toast.LENGTH_SHORT).show()
                }

                ContactDetailsScreenSideEffect.Deleted -> {
                    Toast.makeText(context, context.getString(R.string.deleted_message), Toast.LENGTH_SHORT).show()
                    navController.navigateUp()
                }
            }
        }
    })

    var deleteConfirmationRequired by rememberSaveable { mutableStateOf(false) }

    Column(
        modifier = Modifier
            .padding(16.dp)
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        Box(modifier = Modifier.align(alignment = Alignment.CenterHorizontally)) {
            Image(
                painter = painterResource(R.drawable.contacts_icon),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(100.dp)
                    .clip(CircleShape)
                    .border(
                        width = 2.dp,
                        color = MaterialTheme.colorScheme.onPrimary,
                        shape = CircleShape
                    ),
            )
        }
        ContactInputForm(
            contactUiState = state.changedUiValue,
            onValueChange = { viewModel.changeValue(it) },
            enabled = state.enableEditableState,
        )

        if (state.enableEditableState) {
            OutlinedButton(
                onClick = { viewModel.updateContactDetails() }, modifier = Modifier.fillMaxWidth()
            ) {
                Text(text = stringResource(R.string.update_button))
            }
        } else {
            OutlinedButton(
                onClick = { viewModel.setEditableState(true) }, modifier = Modifier.fillMaxWidth()
            ) {
                Text(text = stringResource(R.string.set_edit_enable_button))
            }
        }

        OutlinedButton(
            onClick = { deleteConfirmationRequired = true }, modifier = Modifier.fillMaxWidth()
        ) {
            Text(text = stringResource(R.string.delete_button))
        }

        if (deleteConfirmationRequired) {
            DeleteConfirmationDialog(
                onDeleteConfirm = { deleteConfirmationRequired = false; viewModel.deleteContact() },
                onDeleteCancel = { deleteConfirmationRequired = false },
                viewModel = viewModel
            )
        }
    }

    if (state.isShowProgress) ProgressBarWithBlock()

}


@Composable
private fun DeleteConfirmationDialog(
    onDeleteConfirm: () -> Unit,
    onDeleteCancel: () -> Unit,
    modifier: Modifier = Modifier,
    viewModel: ContactDetailsViewModel
) {
    val uiState by viewModel.state.collectAsState()

    AlertDialog(onDismissRequest = {  },
        title = { Text(stringResource(R.string.delete_this_contact)) },
        text = { Text("${uiState.data?.fullName} will be removed from your Contacts") },
        modifier = modifier.padding(16.dp),
        dismissButton = {
            TextButton(onClick = onDeleteCancel) {
                Text(stringResource(R.string.cancel))
            }
        },
        confirmButton = {
            TextButton(onClick = onDeleteConfirm) {
                Text(stringResource(R.string.delete))
            }
        })
}