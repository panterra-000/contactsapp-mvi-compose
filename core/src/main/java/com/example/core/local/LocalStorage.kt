package com.example.core.local

import android.content.Context
import android.content.SharedPreferences

class LocalStorage private constructor(context: Context) {
    companion object {

        @Volatile
        lateinit var instance: LocalStorage
            private set

        fun init(context: Context) {
            synchronized(this) {
                instance = LocalStorage(context)
            }
        }
    }

    val pref: SharedPreferences = context.getSharedPreferences("LocalStorage", Context.MODE_PRIVATE)


}
