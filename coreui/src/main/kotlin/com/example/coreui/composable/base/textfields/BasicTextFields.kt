package com.example.coreui.composable.base.textfields

import androidx.annotation.DrawableRes
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.coreui.theme.ContactAppTheme
import com.example.coreui.composable.base.Spacer10dp
import com.example.coreui.composable.base.Spacer15dp
import com.example.coreui.composable.base.texts.Text16Hint


@ExperimentalMaterial3Api
@Composable
fun AppBasicTextField(
    textState: MutableState<String>,
    hintText: String = "",
    @DrawableRes iconId: Int? = null,
    defMarginTop: Boolean = true,
    defMarginBottom: Boolean = false,
) {
    Column {
        if (defMarginTop) Spacer15dp()
        TextField(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(12.dp)),
            value = textState.value,
            colors = TextFieldDefaults.textFieldColors(
                containerColor = ContactAppTheme
                    .colors.primaryInputBackground,
                cursorColor = Color.Black,
                disabledLabelColor = Color.Transparent,
                focusedIndicatorColor = ContactAppTheme
                    .colors.transparent,
                unfocusedIndicatorColor = ContactAppTheme
                    .colors.transparent
            ),
            textStyle = TextStyle(
                color = ContactAppTheme
                    .colors.primaryText, fontSize = 16.sp
            ),
            onValueChange = {
                textState.value = it
            },
            singleLine = true,
            placeholder = {
                Text16Hint(
                    hintText,
                    color = ContactAppTheme
                        .colors.inactiveText
                )
            },
            trailingIcon = {
                if (iconId != null)
                    IconButton(onClick = { }) {
                        Icon(painter = painterResource(id = iconId), contentDescription = null)
                    }
            }
        )
        if (defMarginBottom) Spacer10dp()
    }
}

