package com.example.presentation.viewmodels.create

import androidx.compose.runtime.Immutable
import com.example.presentation.mvibase.UiEvent

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
@Immutable
sealed class CreateContactScreenEvent : UiEvent {

    data class Error(val throwable: Throwable) : CreateContactScreenEvent()
    object CreateSend : CreateContactScreenEvent()
    data class Created(val id: Int) : CreateContactScreenEvent()
    object NavigateUp : CreateContactScreenEvent()

}