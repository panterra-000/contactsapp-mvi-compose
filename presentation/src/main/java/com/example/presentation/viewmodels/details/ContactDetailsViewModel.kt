package com.example.presentation.viewmodels.details

import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.viewModelScope
import com.example.core.data.AppResponse
import com.example.core.repository.ContactRepository
import com.example.presentation.mvibase.BaseViewModel
import com.example.presentation.mvibase.Reducer
import com.example.presentation.util.ContactUiState
import com.example.presentation.util.toContact
import com.example.presentation.util.toContactUiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
@HiltViewModel
class ContactDetailsViewModel @Inject constructor(
    private val repository: ContactRepository,
) : BaseViewModel<ContactDetailsScreenState, ContactDetailsScreenEvent, ContactDetailsScreenSideEffect>() {

    private val reducer = MainReducer(ContactDetailsScreenState.initial())

    override val state: StateFlow<ContactDetailsScreenState> = reducer.state
    override val sideEffect: Flow<ContactDetailsScreenSideEffect> = reducer.sideEffect


    fun getContactDetails(id: Int) = viewModelScope.launch(Dispatchers.IO) {
        sendEvent(ContactDetailsScreenEvent.GetDetailsSend)
        when (val resp = repository.getContact(
            id
        )) {
            is AppResponse.Error -> {
                sendEvent(ContactDetailsScreenEvent.Error(throwable = resp.throwable))
            }

            is AppResponse.Success -> {
                sendEvent(ContactDetailsScreenEvent.TakenDetails(resp.data))
            }
        }
    }

    fun updateContactDetails() = viewModelScope.launch(Dispatchers.IO) {
        sendEvent(ContactDetailsScreenEvent.UpdateSend)
        when (val resp = repository.editContact(state.value.changedUiValue.toContact())) {
            is AppResponse.Error -> {
                sendEvent(ContactDetailsScreenEvent.Error(throwable = resp.throwable))
            }

            is AppResponse.Success -> {
                sendEvent(ContactDetailsScreenEvent.TakenUpdate)
            }
        }
    }

    fun deleteContact() = viewModelScope.launch(Dispatchers.IO) {
        sendEvent(ContactDetailsScreenEvent.DeleteSend)
        when (val resp = repository.deleteContact(state.value.changedUiValue.toContact())) {
            is AppResponse.Error -> {
                sendEvent(ContactDetailsScreenEvent.Error(throwable = resp.throwable))
            }

            is AppResponse.Success -> {
                sendEvent(ContactDetailsScreenEvent.TakenDeleted)
            }
        }
    }

    fun navigateUp() {
        sendEvent(ContactDetailsScreenEvent.NavigateUp)
    }

    fun setEditableState(enable: Boolean) {
        sendEvent(ContactDetailsScreenEvent.SetEditableState(enable))
    }

    private fun sendEvent(event: ContactDetailsScreenEvent) {
        reducer.sendEvent(event)
    }

    fun changeValue(changedValue: ContactUiState) {
        sendEvent(ContactDetailsScreenEvent.ChangeValue(changedValue))
    }

    private class MainReducer(initial: ContactDetailsScreenState) :
        Reducer<ContactDetailsScreenState, ContactDetailsScreenEvent, ContactDetailsScreenSideEffect>(
            initial
        ) {
        override fun reduce(
            oldState: ContactDetailsScreenState,
            event: ContactDetailsScreenEvent
        ) {
            when (event) {
                is ContactDetailsScreenEvent.GetDetailsSend -> {
                    setState(oldState.copy(isShowProgress = true, data = null))
                }

                is ContactDetailsScreenEvent.TakenDetails -> {
                    setState(oldState.copy(isShowProgress = false, data = event.contact, changedUiValue = event.contact.toContactUiState(actionEnable = true)))
                }

                is ContactDetailsScreenEvent.Error -> {
                    setState(oldState.copy(isShowProgress = false))
                    setSideEffect(ContactDetailsScreenSideEffect.ShowError(message = event.throwable.message.toString()))
                }

                ContactDetailsScreenEvent.NavigateUp -> {
                    setSideEffect(ContactDetailsScreenSideEffect.NavigateUp)
                }

                is ContactDetailsScreenEvent.ChangeValue -> {
                    setState(oldState.copy(changedUiValue = event.changedValue))
                }

                is ContactDetailsScreenEvent.SetEditableState -> {
                    setState(oldState.copy(enableEditableState = event.enable))
                }

                ContactDetailsScreenEvent.UpdateSend -> {
                    setState(oldState.copy(isShowProgress = true, enableEditableState = false))
                }

                ContactDetailsScreenEvent.TakenUpdate -> {
                    setState(oldState.copy(isShowProgress = false))
                    setSideEffect(ContactDetailsScreenSideEffect.Updated)
                }

                ContactDetailsScreenEvent.DeleteSend -> {
                    setState(oldState.copy(isShowProgress = true))
                }

                ContactDetailsScreenEvent.TakenDeleted -> {
                    setState(oldState.copy(isShowProgress = false))
                    setSideEffect(ContactDetailsScreenSideEffect.Deleted)
                }
            }
        }
    }
}

