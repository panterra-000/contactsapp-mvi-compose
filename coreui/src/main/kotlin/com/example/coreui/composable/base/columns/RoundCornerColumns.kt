package com.example.coreui.composable.base.columns

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.coreui.theme.ContactAppTheme


@Composable
fun PrimaryRoundedColumn(
    backgroundColor: Color = ContactAppTheme.colors.lightBackground,
    alignCenter: Boolean = true,
    borderRadius: Dp = 8.dp,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(borderRadius))
            .background(backgroundColor)
            .padding(start = 12.dp, top = 12.dp, end = 12.dp),
        horizontalAlignment = if (alignCenter) Alignment.CenterHorizontally else Alignment.Start
    )
    {
        content()
    }
}


@Composable
fun PrimaryRoundedColumnWithPadding(
    backgroundColor: Color = ContactAppTheme.colors.lightBackground,
    alignCenter: Boolean = true,
    borderRadius: Dp = 8.dp,
    horizontalPadding: Dp = 12.dp,
    verticalPadding: Dp = 12.dp,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(borderRadius))
            .background(backgroundColor)
            .padding(horizontal = horizontalPadding, vertical = verticalPadding),
        horizontalAlignment = if (alignCenter) Alignment.CenterHorizontally else Alignment.Start
    )
    {
        content()
    }
}


@Composable
fun PrimaryTopRoundedColumnWithPadding(
    backgroundColor: Color = ContactAppTheme.colors.lightBackground,
    alignCenter: Boolean = true,
    borderRadius: Dp = 24.dp,
    horizontalPadding: Dp = 24.dp,
    verticalPadding: Dp = 24.dp,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(topStart = borderRadius, topEnd = borderRadius))
            .background(backgroundColor)
            .padding(horizontal = horizontalPadding, vertical = verticalPadding),
        horizontalAlignment = if (alignCenter) Alignment.CenterHorizontally else Alignment.Start
    )
    {
        content()
    }
}


@Composable
fun TopRoundedColumnFillMaxSize(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .background(ContactAppTheme.colors.secondaryBackground)
            .clip(RoundedCornerShape(topEnd = 20.dp, topStart = 20.dp))
            .background(ContactAppTheme.colors.primaryBackground)
            .padding(horizontal = 50.dp, vertical = 15.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        content()
    }
}


@Composable
fun BoxScope.ColumnWithContentCenter(
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier.align(Alignment.Center),
        horizontalAlignment = Alignment.CenterHorizontally
    )
    {
        content()
    }
}

@Composable
fun VoyoTopRoundedColumn(
    backgroundColor: Color = ContactAppTheme.colors.primaryBackground,
    alignCenter: Boolean = true,
    padding: Dp = 24.dp,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(topStart = 24.dp, topEnd = 24.dp))
            .background(backgroundColor)
            .padding(padding),
        horizontalAlignment = if (alignCenter) Alignment.CenterHorizontally else Alignment.Start
    )
    {
        content()
    }
}

@Composable
fun VoyoRoundedColumn(
    backgroundColor: Color = ContactAppTheme.colors.primaryBackground,
    alignCenter: Boolean = true,
    padding: Dp = 0.dp,
    content: @Composable ColumnScope.() -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clip(RoundedCornerShape(24.dp))
            .background(backgroundColor)
            .padding(padding),
        horizontalAlignment = if (alignCenter) Alignment.CenterHorizontally else Alignment.Start
    )
    {
        content()
    }
}
