package com.example.coreui.composable.base

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.coreui.theme.ContactAppTheme

@Composable
fun TextWithBackground(
    text: String,
    size: TextUnit = 12.sp,
    color: Color = ContactAppTheme.colors.primaryButtonText
) {
    Text(
        text = text,
        fontSize = size,
        color = color,
        modifier = Modifier
            .clip(RoundedCornerShape(4.dp))
            .background(ContactAppTheme.colors.primaryButtonBackground)
            .padding(2.dp)
    )
}