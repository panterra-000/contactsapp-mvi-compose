package com.example.presentation.viewmodels.create

import com.example.presentation.mvibase.UiSideEffect

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
sealed class CreateContactScreenSideEffect : UiSideEffect {

    data class ShowError(val message: String) : CreateContactScreenSideEffect()

    object NavigateUp : CreateContactScreenSideEffect()
    object Created : CreateContactScreenSideEffect()

}