package com.example.presentation.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavHostController
import com.example.presentation.ContactsTopAppBar
import kotlinx.coroutines.launch

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
//@ExperimentalMaterial3Api
//@Composable
//fun EditContactScreen(
//    navController: NavHostController,
//) {
//    val coroutineScope = rememberCoroutineScope()
//
//    Scaffold(topBar = {
//        ContactsTopAppBar(
//            title = stringResource(EditScreenDestination.titleRes),
//            navigateBack = true,
//            navigateUp = onNavigateUp
//        )
//    }) { innerPadding ->
//        EntryBody(
//            contactUiState = viewModel.contactUiState,
//            onContactValueChange = viewModel::updateUiState,
//            onSaveClick = {
//                coroutineScope.launch {
//                    viewModel.updateContact()
//                    navigateBack()
//                }
//            },
//            modifier = modifier
//                .padding(innerPadding)
//                .background(MaterialTheme.colorScheme.background)
//        )
//    }
//}