package com.example.presentation.viewmodels.contacts

import com.example.core.database.entity.Contact
import com.example.presentation.mvibase.UiState

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
data class ContactsScreenState(
    val isShowProgress: Boolean = false,
    val error: Throwable? = null,
    val data: List<Contact>
) : UiState {

    companion object {
        fun initial() = ContactsScreenState(
            isShowProgress = true,
            data = emptyList(),
            error = null
        )
    }




}