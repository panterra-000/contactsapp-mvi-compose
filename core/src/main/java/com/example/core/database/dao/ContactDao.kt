package com.example.core.database.dao

import androidx.room.Dao
import androidx.room.Query
import com.example.core.database.entity.Contact

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */

@Dao
interface ContactDao : BaseDao<Contact> {

    @Query("SELECT * FROM contact_table")
    suspend fun getAllContact(): List<Contact>

    @Query("SELECT * FROM contact_table WHERE id = :id")
    fun getContact(id: Int): Contact

}