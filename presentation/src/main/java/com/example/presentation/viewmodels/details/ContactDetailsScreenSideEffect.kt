package com.example.presentation.viewmodels.details

import com.example.presentation.mvibase.UiSideEffect

/**
 * Created by Jasurbek Kurganbayev 01/08/2023
 */
sealed class ContactDetailsScreenSideEffect : UiSideEffect {

    data class ShowError(val message: String) : ContactDetailsScreenSideEffect()

    object NavigateUp : ContactDetailsScreenSideEffect()
    object Updated : ContactDetailsScreenSideEffect()
    object Deleted : ContactDetailsScreenSideEffect()

}