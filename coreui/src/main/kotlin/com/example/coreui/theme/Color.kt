package com.example.coreui.theme

import androidx.compose.ui.graphics.Color

object ThemeColor {

    fun getColors(darkTheme: Boolean): ContactAppColors {
        return if (darkTheme) {
            themeDark
        } else {
            themeLight
        }
    }

    private val themeLight = ContactAppColors(
        statusBar = Color(0xFFD0BCFF),
        secondaryStatusBar = Color(0xFFD0BCFF),
        primaryAppbar = Color(0xFFD0BCFF),
        SecondaryAppbar = Color(0xFFD0BCFF),

        primaryBackground = Color(0xFFD0BCFF),
        secondaryBackground = Color(0xFFD0BCFF),
        primaryButtonBackground = Color(0xFFD0BCFF),
        disabledButtonBackground = Color(0xFFD0BCFF),
        secondaryButtonBackground = Color(0xFFD0BCFF),
        inactiveButtonBackground = Color(0xFFD0BCFF),
        contactPlaceHolderBackground = Color(0xFFD0BCFF),
        disabled = Color(0xFFD0BCFF),
        stopButtonBackground = Color(0xFFD0BCFF),

        primaryText = Color(0xFFD0BCFF),
        positiveText = Color(0xFFD0BCFF),
        titleText = Color(0xFFD0BCFF),
        primaryClickableText = Color(0xFFD0BCFF),
        secondaryText = Color(0xFFD0BCFF),
        inactiveText = Color(0xFFD0BCFF),
        hintText = Color(0xFFD0BCFF),
        primaryButtonText = Color(0xFFD0BCFF),
        clearPrimaryButtonText = Color(0xFFD0BCFF),
        secondaryButtonText = Color(0xFFD0BCFF),
        testResultText = Color(0xFFD0BCFF),
        advertisingText = Color(0xFFD0BCFF),
        errorText = Color(0xFFD0BCFF),
        approvedStatusText = Color(0xFFD0BCFF),
        pendingStatusText = Color(0xFFD0BCFF),
        rejectedStatusText = Color(0xFFD0BCFF),

        primaryDivider = Color(0xFFD0BCFF),
        transparent60 = Color(0xFFD0BCFF),
        transparentResult = Color(0xFFD0BCFF),
        transparent = Color(0xFFD0BCFF),
        nonActionText = Color(0xFFD0BCFF),
        primaryInputBackground = Color(0xFFD0BCFF),
        secondaryInputBackground = Color(0xFFD0BCFF),
        mapInputBackground = Color(0xFFD0BCFF),
        selectedFileBackground = Color(0xFFD0BCFF),
        badgeAvatarBackground = Color(0xFFD0BCFF),
        defaultPlasticCardBackground = Color(0xFFD0BCFF),
        plasticCardTitleText = Color(0xFFD0BCFF),
        plasticCardNumberText = Color(0xFFD0BCFF),
        plasticCardDateText = Color(0xFFD0BCFF),
        plasticCardCVCText = Color(0xFFD0BCFF),
        stripeTitleText = Color(0xFFD0BCFF),
        homeTopBarBackground = Color(0xFFD0BCFF),
        homeTopBarCenterGradient = Color(0xFFD0BCFF),
        homeTopBarCenter2Gradient = Color(0xFFD0BCFF),
        homeTopBarBottomGradient = Color(0xFFD0BCFF),
        sidebarBackground = Color(0xFFD0BCFF),
        orderItemBackground = Color(0xFFD0BCFF),
        acceptedDriverItemBackground = Color(0xFFD0BCFF),
        switchThumbColor = Color(0xFFD0BCFF),
        checkedTrackColor = Color(0xFFD0BCFF),
        unCheckedTrackColor = Color(0xFFD0BCFF),
        cardItemInactiveTitleText = Color(0xFFD0BCFF),
        supportBodyBackground = Color(0xFFD0BCFF),
        labeledButtonBorderTint = Color(0xFFD0BCFF),
        secondaryBodyBackground = Color(0xFFD0BCFF),
        lightBackground = Color(0xFFD0BCFF),
        videoFrameBackground = Color(0xFFD0BCFF),
        bottomSheetBackground = Color(0xFFD0BCFF),
        bottomSheetSecondaryBackground = Color(0xFFD0BCFF),
        bottomSheetDivider = Color(0xFFD0BCFF),
        variantButtonBackground = Color(0xFFD0BCFF),
        inactiveBorder = Color(0xFFD0BCFF),
        activeBorder = Color(0xFFD0BCFF),
        mapPlaceListBackground = Color(0xFFD0BCFF),
        secondaryDivider = Color(0xFFD0BCFF),
        mapRouteLineTint = Color(0xFFD0BCFF),
        driverWaveTint = Color(0xFFD0BCFF),
        riderWaveTint = Color(0xFFD0BCFF),
        recentLocationItemBackground = Color(0xFFD0BCFF),
        orderTimeText = Color(0xFFD0BCFF),
        durationLineTint = Color(0xFFD0BCFF),
        durationLineSecondTint = Color(0xFFD0BCFF),
        arrivedSignalBackground = Color(0xFFD0BCFF),
        overlayFoggyTransparentTint = Color(0xFFD0BCFF),
        finishSwipeButtonBackground = Color(0xFFD0BCFF),
        chatBackGround = Color(0xFFD0BCFF),
        chatTextFieldBackGround = Color(0xFFD0BCFF),
        chatSentMessageTextColor = Color(0xFFD0BCFF),
        chatSentMessageBackground = Color(0xFFD0BCFF),
        chatReceivedMessageBackground = Color(0xFFD0BCFF),
        chatReceivedMessageTextColor = Color(0xFFD0BCFF),
        chatSentMessageTimeColor = Color(0xFFD0BCFF),
        chatReceivedMessageTimeColor = Color(0xFFD0BCFF),
        chatUnRideMessagesCountBackground = Color(0xFFD0BCFF),
        disabledMessageBackground = Color(0xFFD0BCFF),
        disableText = Color(0xFFD0BCFF),
    )

    private val themeDark = themeLight
}