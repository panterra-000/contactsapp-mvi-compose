package com.example.presentation.screens


import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Button
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.example.coreui.R
import com.example.coreui.composable.base.ProgressBarWithBlock
import com.example.presentation.ContactsTopAppBar
import com.example.presentation.util.ContactUiState
import com.example.presentation.viewmodels.create.CreateContactScreenSideEffect
import com.example.presentation.viewmodels.create.CreateContactViewModel

/**
 * Created by Jasurbek Kurganbayev 31/07/2023
 */
@ExperimentalMaterial3Api
@Composable
fun CreateContactScreen(
    navController: NavHostController, viewModel: CreateContactViewModel = hiltViewModel()
) {

    val context = LocalContext.current

    val state by viewModel.state.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.sideEffect.collect {
            when (it) {
                CreateContactScreenSideEffect.Created -> {
                    Toast.makeText(
                        context,
                        context.getString(R.string.created_message),
                        Toast.LENGTH_SHORT
                    ).show()
                    navController.navigateUp()
                }

                CreateContactScreenSideEffect.NavigateUp -> {
                    navController.navigateUp()
                }

                is CreateContactScreenSideEffect.ShowError -> {
                    Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
    })


    Scaffold(topBar = {
        ContactsTopAppBar(title = stringResource(R.string.create_contact_screen_title),
            navigateBack = true,
            navigateUp = { viewModel.navigateUp() })
    }) { innerPadding ->
        EntryBody(
            contactUiState = viewModel.contactUiState.value,
            onContactValueChange = { viewModel.updateUiState(it) },
            onSaveClick = {
                viewModel.createContact()
            },
            modifier = Modifier
                .padding(innerPadding)
                .background(MaterialTheme.colorScheme.background)
        )

        if (state.isShowProgress) ProgressBarWithBlock()

    }
}

@ExperimentalMaterial3Api
@Composable
fun EntryBody(
    contactUiState: ContactUiState,
    onContactValueChange: (ContactUiState) -> Unit,
    onSaveClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(16.dp),
        verticalArrangement = Arrangement.spacedBy(32.dp)
    ) {
        Box(modifier = modifier.align(alignment = Alignment.CenterHorizontally)) {
            Image(
                painter = painterResource(R.drawable.contacts_icon),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(100.dp)
                    .clip(CircleShape)
                    .border(
                        width = 2.dp,
                        color = MaterialTheme.colorScheme.onPrimary,
                        shape = CircleShape
                    ),
            )
        }

        ContactInputForm(
            contactUiState = contactUiState, onValueChange = onContactValueChange
        )
        Button(
            onClick = onSaveClick,
            enabled = contactUiState.actionEnable,
            modifier = modifier.fillMaxWidth()
        ) {
            Text(text = stringResource(R.string.save_contact))
        }
    }
}

@ExperimentalMaterial3Api
@Composable
fun ContactInputForm(
    contactUiState: ContactUiState,
    modifier: Modifier = Modifier,
    onValueChange: (ContactUiState) -> Unit = {},
    enabled: Boolean = true
) {
    Column(
        modifier = modifier.fillMaxWidth(), verticalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        TextField(
            value = contactUiState.fullName,
            onValueChange = { onValueChange(contactUiState.copy(fullName = it)) },
            label = { Text(stringResource(R.string.contact_name_input)) },
            modifier = modifier.fillMaxWidth(),
            enabled = enabled,
            textStyle = TextStyle(color = Color.Black),
            singleLine = true,
            colors = TextFieldDefaults.textFieldColors(
                containerColor = Color.White,
                focusedIndicatorColor = Color.Transparent,
            )
        )
        contactUiState.mobile?.let {
            TextField(
                value = it,
                onValueChange = { onValueChange(contactUiState.copy(mobile = it)) },
                keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                label = { Text(stringResource(R.string.contact_phone_input)) },
                modifier = modifier.fillMaxWidth(),
                enabled = enabled,
                textStyle = TextStyle(color = Color.Black),
                singleLine = true,
                colors = TextFieldDefaults.textFieldColors(
                    containerColor = Color.White,
                    focusedIndicatorColor = Color.Transparent,
                )
            )
        }
        contactUiState.email?.let {
            TextField(
                value = it,
                onValueChange = { onValueChange(contactUiState.copy(email = it)) },
                label = { Text(stringResource(R.string.contact_email_input)) },
                modifier = modifier.fillMaxWidth(),
                enabled = enabled,
                textStyle = TextStyle(color = Color.Black),
                singleLine = true,
                colors = TextFieldDefaults.textFieldColors(
                    containerColor = Color.White,
                    focusedIndicatorColor = Color.Transparent,
                )
            )
        }
    }
}

